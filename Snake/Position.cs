﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    class Position
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string toBeDisplayed { get; set; }
        public Position(int x, int y, string display)
        {
            this.toBeDisplayed = display;
            this.X = x;
            this.Y = y;
        }

        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }
            if(!(obj is Position))
            {
                return false;
            }
            if(this.X == ((Position)obj).X && this.Y == ((Position)obj).Y && this.toBeDisplayed == ((Position)obj).toBeDisplayed)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return this.toBeDisplayed.GetHashCode();
        }
    }
}
