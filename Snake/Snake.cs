﻿using System;
using System.Collections.Generic;

namespace Snake
{
    class Snake
    {
        public List<Position> positions = new List<Position>();
        public byte Direction{ get; set; } //1 is right, 2 up, 3 left, 4 down
        public Snake()
        {
            this.positions.Add(new Position(5, 5, "0"));
            this.positions.Add(new Position(4, 5, "o"));
            this.positions.Add(new Position(3, 5, "o"));
            this.Direction = 1;
        }
    }
}
