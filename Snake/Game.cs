﻿using System;
using System.Linq;
using System.Threading;
using System.Xml.Linq;

namespace Snake
{
    class Game
    {
        private string pitch = "";

        private Snake snake = new Snake();

        private Food food = new Food();

        private int PreviousHighScore;
        private int Point;
        private bool isOver;

        public Game()
        {
            Console.SetWindowSize(42, 20);
            bool quit = false;
            this.PreviousHighScore = GetPreviousRecord();
            this.isOver = false;
            this.Point = 0;
            do
            {
                CheckFoodPosition();
                Console.Clear();
                do
                {
                    switch (this.snake.Direction)
                    {
                        case 1: case 3:
                            Thread.Sleep(90);
                            break;
                        case 2: case 4:
                            Thread.Sleep(125);
                            break;
                    }
                    InitializePitch();
                    Display();
                    ScanKeyboard();
                    OneStep();
                } while (!this.isOver);
                this.isOver = false;
                this.pitch = "";
                this.snake = new Snake();
                this.food = new Food();
                Console.WriteLine("\nGame Over");
                if (this.Point > this.PreviousHighScore)
                {
                    this.PreviousHighScore = this.Point;
                    
                    Console.WriteLine("New Record!");
                    XDocument xdoc = XDocument.Load("record.xml");
                    XElement element = (from x in xdoc.Descendants("Record")
                                        where x.Element("GameMode").Value == "Snake"
                                        select x.Element("HighScore")).FirstOrDefault();
                    element.Value = this.PreviousHighScore.ToString();
                    xdoc.Save("record.xml");
                }
                this.Point = 0;
                Console.WriteLine("Press enter to play again, escape to exit snake!");
            Input:
                var key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.Escape:
                        quit = true;
                        break;
                    case ConsoleKey.LeftArrow: case ConsoleKey.UpArrow: case ConsoleKey.DownArrow: case ConsoleKey.RightArrow: case ConsoleKey.W: case ConsoleKey.A: case ConsoleKey.D: case ConsoleKey.S:
                        goto Input;
                }
            } while (!quit);
        }

        private void ScanKeyboard()
        {
            if (Console.KeyAvailable)
            {
                var key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.UpArrow: case ConsoleKey.W:
                        if(this.snake.Direction != 4)
                        {
                            this.snake.Direction = 2;
                        }
                        break;
                    case ConsoleKey.DownArrow: case ConsoleKey.S:
                        if(this.snake.Direction != 2)
                        {
                            this.snake.Direction = 4;
                        }
                        break;
                    case ConsoleKey.LeftArrow: case ConsoleKey.A:
                        if (this.snake.Direction != 1)
                        {
                            this.snake.Direction = 3;
                        }
                        break;
                    case ConsoleKey.RightArrow: case ConsoleKey.D:
                        if(this.snake.Direction != 3)
                        {
                            this.snake.Direction = 1;
                        }
                        break;
                }
            }
        }

        private void OneStep()
        {
            var item = IsThereASnake(this.food.position.X, this.food.position.Y);
            if(!item.Equals(this.food.position))
            {
                this.Point++;
                this.snake.positions.Add(new Position(this.snake.positions[this.snake.positions.Count - 1].X-1, this.snake.positions[this.snake.positions.Count - 1].Y-1, "o"));
                CheckFoodPosition();
            }

            for (int i = this.snake.positions.Count - 1; i > 0; i--)
            {
                this.snake.positions[i].X = this.snake.positions[i - 1].X;
                this.snake.positions[i].Y = this.snake.positions[i - 1].Y;
            }
            switch (this.snake.Direction)
            {
                case 1:
                    if (this.snake.positions[0].X != 37)
                    {
                        this.snake.positions[0].X++;
                    }
                    else
                    {
                        this.snake.positions[0].X = 0;
                    }
                    break;
                case 2:
                    if(this.snake.positions[0].Y != 0)
                    {
                        this.snake.positions[0].Y--;
                    }
                    else
                    {
                        this.snake.positions[0].Y = 9;
                    }
                    break;
                case 3:
                    if (this.snake.positions[0].X != 0)
                    {
                        this.snake.positions[0].X--;
                    }
                    else
                    {
                        this.snake.positions[0].X = 37;
                    }
                    break;
                case 4:
                    if (this.snake.positions[0].Y != 9)
                    {
                        this.snake.positions[0].Y++;
                    }
                    else
                    {
                        this.snake.positions[0].Y = 0;
                    }
                    break;
            }
            item = IsThereASnake(this.snake.positions[0].X, this.snake.positions[0].Y, true);
            if (item != null && !item.Equals(this.food.position))
            {
                this.isOver = true;
            }
        }

        private void InitializePitch()
        {
            this.pitch = "";
            for (int i = 0; i < 40; i++)
            {
                pitch += "*";
            }
            pitch += "\n";
            for (int i = 0; i < 10; i++)
            {
                pitch += "*";
                for (int j= 0; j < 38; j++)
                {
                    Position item = IsThereASnake(j, i);
                    if(item != null)
                    {
                        pitch += item.toBeDisplayed;
                    }
                    else
                    {
                        pitch += " ";
                    }
                }
                pitch += "*\n";
            }
            for (int i = 0; i < 40; i++)
            {
                pitch += "*";
            }
        }

        private Position IsThereASnake(int x, int y)
        {
            return IsThereASnake(x, y, false);
        }

        private Position IsThereASnake(int x, int y, bool snakeHead)
        {
            foreach (var item in this.snake.positions)
            {
                if (item.X == x && item.Y == y)
                {
                    if (snakeHead)
                    {
                        if (!item.Equals(this.snake.positions[0]))
                        {
                            return item;
                        }
                    }
                    else
                    {
                        return item;
                    }
                }
            }
            if (this.food.position.X == x && this.food.position.Y == y)
            {
                return this.food.position;
            }
            return null;
        }
        private void CheckFoodPosition()
        {
            Position item;
            do
            {
                this.food.GenPosition();
                item = IsThereASnake(food.position.X, food.position.Y);
            } while (!item.Equals(this.food.position));
        }

        private void Display()
        {
            this.pitch += "\nYour point : " + this.Point + "\n High Score: " + this.PreviousHighScore;
            Console.SetCursorPosition(0, 0);
            Console.Write(this.pitch);
        }

        private int GetPreviousRecord()
        {
            XDocument xdoc = XDocument.Load("record.xml");
            int previous = int.Parse((from x in xdoc.Descendants("Record")
                                      where x.Element("GameMode").Value == "Snake"
                                      select x.Element("HighScore")).FirstOrDefault().Value);
            return previous;
        }
    }
}
