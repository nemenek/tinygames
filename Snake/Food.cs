﻿using System;

namespace Snake
{
    class Food
    {
        private Random rnd = new Random();
        public Position position { get; set; }
        public Food()
        {
            this.position = new Position(0, 0, "*");
        }

        public void GenPosition()
        {
            this.position.X = rnd.Next(0, 28);
            this.position.Y = rnd.Next(0, 10);
        }
    }
}
