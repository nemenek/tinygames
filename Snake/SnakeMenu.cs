﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    public class SnakeMenu
    {
        public SnakeMenu()
        {
            Menu();
        }

        private void Menu()
        {
            bool back = false;
            while (!back){
                Console.Clear();
                Console.WriteLine("Welcome to Snake!");
                Console.WriteLine("ESC - Back");
                Console.WriteLine("Press any other key to start...");
                var key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.Escape:
                        back = true;
                        break;
                    default:
                        Game game = new Game();
                        Console.SetWindowSize(120, 30);
                        back = true;
                        break;
                }
            }
        }
    }
}
