﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using Flappy_Bird;
using Snake;
using Pac_Man;

namespace BigOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            if (!File.Exists("record.xml"))
            {
                CreateXml();
            }
            Console.OutputEncoding = Encoding.Unicode;
            Menu();
        }

        private static void CreateXml()
        {
            XmlWriterSettings setting = new XmlWriterSettings
            {
                Indent = true,
                //setting.OmitXmlDeclaration = true;
                NewLineOnAttributes = true
            };
            using (XmlWriter writer = XmlWriter.Create("record.xml", setting))
            {
                writer.WriteStartElement("Records");
                writer.WriteStartElement("Record");
                writer.WriteElementString("GameMode", "Easy");
                writer.WriteElementString("HighScore", "0");
                writer.WriteEndElement();
                writer.WriteStartElement("Record");
                writer.WriteElementString("GameMode", "Normal");
                writer.WriteElementString("HighScore", "0");
                writer.WriteEndElement();
                writer.WriteStartElement("Record");
                writer.WriteElementString("GameMode", "Hard");
                writer.WriteElementString("HighScore", "0");
                writer.WriteEndElement();
                writer.WriteStartElement("Record");
                writer.WriteElementString("GameMode", "Impossible");
                writer.WriteElementString("HighScore", "0");
                writer.WriteEndElement();
                writer.WriteStartElement("Record");
                writer.WriteElementString("GameMode", "Level1");
                writer.WriteElementString("Completed", "false");
                writer.WriteEndElement();
                writer.WriteStartElement("Record");
                writer.WriteElementString("GameMode", "Level2");
                writer.WriteElementString("Completed", "false");
                writer.WriteEndElement();
                writer.WriteStartElement("Record");
                writer.WriteElementString("GameMode", "Level3");
                writer.WriteElementString("Completed", "false");
                writer.WriteEndElement();
                writer.WriteStartElement("Record");
                writer.WriteElementString("GameMode", "Level4");
                writer.WriteElementString("Completed", "false");
                writer.WriteEndElement();
                writer.WriteStartElement("Record");
                writer.WriteElementString("GameMode", "Snake");
                writer.WriteElementString("HighScore", "0");
                writer.WriteElementString("Recorder", "");
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.Flush();
            }
        }

        public static void Menu()
        {
            bool exit = false;
            while (!exit)
            {
                Console.Clear();
                Console.WriteLine("Hello,");
                Console.WriteLine("Choose a game please!");
                Console.WriteLine("1. Flappy Bird");
                Console.WriteLine("2. Snake");
                Console.WriteLine("3. Pac-Man");
                Console.WriteLine("ESC - Exit");
                var key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        Flappy_Menu flappy = new Flappy_Menu();
                        break;
                    case ConsoleKey.D2: case ConsoleKey.NumPad2:
                        SnakeMenu snake = new SnakeMenu();
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        Pac_ManMenu pacman = new Pac_ManMenu();
                        break;
                    case ConsoleKey.Escape:
                        exit = true;
                        break;
                }
            }
        }
        // TODO: Flappy: save recorder name
        //      Snake, pacman, doodle jump, Tetris, spaceship shooter
        //      cripto system to write and read record
    }
}
