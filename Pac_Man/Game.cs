﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pac_Man
{
    class Game
    {
        private int Point;

        private List<Entity> entities;

        private Entity loggerEntity;

        private Dictionary<int, char> map = new Dictionary<int, char>();

        private List<Node> Graph;

        private byte TempDirection;
        private byte WaitingTime;

        private int counter;
        private bool isOver;
        private int maxpoint;

        Thread[] threads = new Thread[4];

        public Game()
        {
            this.maxpoint = 0;
            try
            {
                this.loggerEntity = new PacMan(-1, ' ', ConsoleColor.Black);
                SetUpGame();
                do
                {
                    Thread.Sleep(60);
                    RefreshMap();
                    Display();
                    ScanKeyboard();
                    OneStep();
                } while (!this.isOver);
                RefreshMap();
                Display();
                if(this.Point == this.maxpoint)
                {
                    Console.WriteLine("Congratulations! You Won!");
                }
                else
                {
                    Console.WriteLine("Game Over");
                }
                this.loggerEntity.OnLogging(new LoggerEvent("Game finished. Score: " + this.Point));
                Console.ReadLine();
            }
            catch (Exception e)
            {
                this.loggerEntity.OnLogging(new LoggerEvent("Error: " + e.Message));
                Console.WriteLine("An error occured, check log for further information.");
                Console.ReadLine();
            }
        }

        private void SetUpGame()
        {
            this.counter = 0;
            this.isOver = false;
            this.WaitingTime = 0;
            this.entities = new List<Entity>();
            entities.Add(new PacMan());
            entities.Add(new OrangeGhost());
            entities.Add(new RedGhost());
            entities.Add(new PinkGhost());
            entities.Add(new BlueGhost());
            this.Point = 0;
            InitializeMap();
            initializeGraph();
            
            loggerEntity.OnLogging(new LoggerEvent("SetUp finished."));
        }

        private void ScanKeyboard()
        {
            if (Console.KeyAvailable)
            {
                var key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.UpArrow:
                    case ConsoleKey.W:
                        this.TempDirection = 2;
                        break;
                    case ConsoleKey.DownArrow:
                    case ConsoleKey.S:
                        this.TempDirection = 4;
                        break;
                    case ConsoleKey.LeftArrow:
                    case ConsoleKey.A:
                        this.TempDirection = 3;
                        break;
                    case ConsoleKey.RightArrow:
                    case ConsoleKey.D:
                        this.TempDirection = 1;
                        break;
                }
            }
        }

        private Entity GetPacMan()
        {
            var pacman = (from x in this.entities
                          where x.GetDisplay() == 'C' || x.GetDisplay() == 'Ɔ'
                          select x).FirstOrDefault();
            return pacman;
        }

        private void InitializeMap()
        {
            this.map.Clear();
            string[] lines = File.ReadAllLines("map.txt");
            Console.WriteLine();
            int idx = 0;
            foreach (var item in lines)
            {
                for (int i = 0; i < item.Length; i++)
                {
                    map.Add(idx, item[i]);
                    idx++;
                    if (item[i] == '*')
                    {
                        this.maxpoint++;
                    }
                }
                map.Add(idx, '\n');
                idx++;
            }
            maxpoint--;
        }

        private void RefreshMap()
        {
            foreach (var item in this.entities)
            {
                map[item.GetPosition()] = item.GetDisplay();
            }
        }

        private void Display()
        {
            Console.SetCursorPosition(0, 0);
            bool setColor = false;
            foreach (var item in this.map)
            {
                if (this.entities.Where(x => x.GetPosition() == item.Key).Select(x => x).FirstOrDefault() != null && Console.ForegroundColor != entities.Where(x => x.GetPosition() == item.Key).Select(x => x).FirstOrDefault().Color)
                {
                    Console.ForegroundColor = entities.Where(x => x.GetPosition() == item.Key).Select(x => x).FirstOrDefault().Color;
                    setColor = true;
                }
                else if (item.Value == '*')
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    setColor = true;
                }
                Console.Write(item.Value);
                if (setColor)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    setColor = false;
                }
            }
            Console.WriteLine("Point: " + this.Point);
        }

        private void OneStep()
        {
            var pacman = GetPacMan();

            switch (this.TempDirection)
            {
                case 1:
                    if (map[pacman.GetPosition() + 2] != '■')
                    {
                        if (map[pacman.GetPosition() + 2] == '*')
                        {
                            Point++;
                        }
                        if (pacman.GetPosition() != 452)
                        {
                            pacman.SetDirection(this.TempDirection);
                            pacman.Step(this.map, pacman, this.Graph);
                        }
                        else
                        {
                            map[pacman.GetPosition()] = ' ';
                            if (map[420] == '*')
                            {
                                this.Point++;
                            }
                            pacman.SetPosition(420); //Nice
                        }
                        pacman.SetDisplay('C');
                    }
                    else
                    {
                        OneStep(pacman);
                        this.WaitingTime++;
                        if (this.WaitingTime == 5)
                        {
                            this.TempDirection = pacman.GetDirection();
                            this.WaitingTime = 0;
                        }
                    }
                    break;
                case 2:
                    if (map[pacman.GetPosition() - 38] != '■')
                    {
                        if (map[pacman.GetPosition() - 38] == '*')
                        {
                            this.Point++;
                        }
                        pacman.SetDirection(this.TempDirection);
                        pacman.Step(this.map, pacman, this.Graph);
                    }
                    else
                    {
                        OneStep(pacman);
                        this.WaitingTime++;
                        if (this.WaitingTime == 5)
                        {
                            this.TempDirection = pacman.GetDirection();
                            this.WaitingTime = 0;
                        }
                    }
                    break;
                case 3:
                    if (map[pacman.GetPosition() - 2] != '■')
                    {
                        if (map[pacman.GetPosition() - 2] == '*')
                        {
                            this.Point++;
                        }
                        if (pacman.GetPosition() != 420)
                        {
                            pacman.SetDirection(this.TempDirection);
                            pacman.Step(this.map, pacman, this.Graph);
                        }
                        else
                        {
                            map[pacman.GetPosition()] = ' ';
                            if (map[454] == '*')
                            {
                                this.Point++;
                            }
                            pacman.SetPosition(454);
                        }
                        pacman.SetDisplay('Ɔ');
                    }
                    else
                    {
                        OneStep(pacman);
                        this.WaitingTime++;
                        if (this.WaitingTime == 5)
                        {
                            this.TempDirection = pacman.GetDirection();
                            this.WaitingTime = 0;
                        }
                    }
                    break;
                case 4:
                    if (map[pacman.GetPosition() + 38] != '■')
                    {
                        if (map[pacman.GetPosition() + 38] == '*')
                        {
                            this.Point++;
                        }
                        pacman.SetDirection(this.TempDirection);
                        pacman.Step(this.map, pacman, this.Graph);
                    }
                    else
                    {
                        OneStep(pacman);
                        this.WaitingTime++;
                        if (this.WaitingTime == 5)
                        {
                            this.TempDirection = pacman.GetDirection();
                            this.WaitingTime = 0;
                        }
                    }
                    break;
                default:

                    break;
            }
            this.loggerEntity.OnLogging(new LoggerEvent("Point: " + this.Point));
            if (this.Point == this.maxpoint)
            {
                this.isOver = true;
            }
            else if (this.Point == 1)
            {
                var pinky = (from x in this.entities
                             where x.Color == ConsoleColor.Magenta
                             select x).FirstOrDefault();
                pinky.SetStarted(true, this.map);
            }
            else if (this.Point == 30)
            {
                var blue = (from x in this.entities
                            where x.Color == ConsoleColor.Blue
                            select x).FirstOrDefault();
                blue.SetStarted(true, this.map);
            }
            else if (this.Point == 60)
            {
                var orange = (from x in this.entities
                              where x.Color == ConsoleColor.DarkYellow
                              select x).FirstOrDefault();
                orange.SetStarted(true, this.map);
            }

            var enemies = from x in entities
                          where x.GetDisplay() != 'C' && x.GetDisplay() != 'Ɔ'
                          select x;

            bool ghostStep = false;

            if (counter == 1 && !this.isOver)
            {
                ghostStep = true;
            }

            foreach (var entity in enemies)
            {
                if (ghostStep)
                {
                    if (entity.GetPosition() == pacman.GetPosition())
                    {
                        this.isOver = true;
                    }
                    else
                    {
                        if (entity.Color == ConsoleColor.Blue)
                        {
                            BlueGhost entity1 = (BlueGhost)entity;
                            entity1.Step(this.map, pacman, this.Graph, enemies.Where(x => x.Color == ConsoleColor.Red).FirstOrDefault().GetPosition());
                        }
                        else
                        {
                            entity.Step(this.map, pacman, this.Graph);
                        }
                        this.counter = 0;
                        if (entity.GetPosition() == pacman.GetPosition())
                        {
                            this.isOver = true;
                        }
                    }
                }
                else
                {
                    this.counter = 1;
                    if (entity.GetPosition() == pacman.GetPosition())
                    {
                        this.isOver = true;
                    }
                }
            }
        }

        private void OneStep(Entity pacman)
        {
            switch (pacman.GetDirection())
            {
                case 1:
                    if (map[pacman.GetPosition() + 2] != '■')
                    {
                        if (map[pacman.GetPosition() + 2] == '*')
                        {
                            Point++;
                        }
                        if (pacman.GetPosition() != 452)
                        {
                            pacman.Step(this.map, pacman, this.Graph);
                        }
                        else
                        {
                            map[pacman.GetPosition()] = ' ';
                            if (map[420] == '*')
                            {
                                this.Point++;
                            }
                            pacman.SetPosition(420); //Nice
                        }
                    }
                    else
                    {
                        pacman.SetDirection(0);
                    }
                    break;
                case 2:
                    if (map[pacman.GetPosition() - 38] != '■')
                    {
                        if (map[pacman.GetPosition() - 38] == '*')
                        {
                            this.Point++;
                        }
                        pacman.Step(this.map, pacman, this.Graph);
                    }
                    else
                    {
                        pacman.SetDirection(0);
                    }
                    break;
                case 3:
                    if (map[pacman.GetPosition() - 2] != '■')
                    {
                        if (map[pacman.GetPosition() - 2] == '*')
                        {
                            this.Point++;
                        }
                        if (pacman.GetPosition() != 420)
                        {
                            pacman.Step(this.map, pacman, this.Graph);
                        }
                        else
                        {
                            map[pacman.GetPosition()] = ' ';
                            if (map[454] == '*')
                            {
                                this.Point++;
                            }
                            pacman.SetPosition(454);
                        }
                    }
                    else
                    {
                        pacman.SetDirection(0);
                    }
                    break;
                case 4:
                    if (map[pacman.GetPosition() + 38] != '■')
                    {
                        if (map[pacman.GetPosition() + 38] == '*')
                        {
                            this.Point++;
                        }
                        pacman.Step(this.map, pacman, this.Graph);
                    }
                    else
                    {
                        pacman.SetDirection(0);
                    }
                    break;
                default:

                    break;
            }
        }

        private void initializeGraph()
        {
            this.Graph = new List<Node>();
            int counter = 0;
            for (int i = 0; i < map.Count; i++)
            {
                if (map[i] == '*')
                {
                    if (map[i + 2] == '*') counter++;
                    if (map[i - 2] == '*') counter++;
                    if (map[i + 38] == '*') counter++;
                    if (map[i - 38] == '*') counter++;
                    if (counter >= 3)
                    {
                        this.Graph.Add(new Node(i));
                    }
                    counter = 0;
                }
            }
            foreach (var item in this.Graph)
            {
                for (int i = 1; i < 5; i++)
                {
                    FindNeighbours(item, item.ID, i, 0, Convert.ToByte(i));
                }
            }
            ;
        }

        private void FindNeighbours(Node item, int idx, int direction, int steps, byte baseDirection)
        {
            switch (direction)
            {
                case 1:
                    if (map[idx - 2] != '■')
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx - 2).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx - 2, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx - 2, 0, steps, baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx - 2, 1, steps, baseDirection);
                        }
                    }
                    else if (map[idx - 38] != '■' && steps != 0)
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx - 38).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx - 38, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx - 38, 0, steps, baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx - 38, 4, steps, baseDirection);
                        }
                    }
                    else if (map[idx + 38] != '■' && steps != 0)
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx + 38).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx + 38, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx - 38, 0, steps, baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx + 38, 2, steps, baseDirection);
                        }
                    }
                    break;
                case 2:
                    if (map[idx + 38] != '■')
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx + 38).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx + 38, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx - 38, 0, steps, baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx + 38, 2, steps, baseDirection);
                        }
                    }
                    else if (map[idx - 2] != '■' && steps != 0)
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx - 2).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx - 2, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx - 2, 0, steps,baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx - 2, 1, steps, baseDirection);
                        }
                    }
                    else if (map[idx + 2] != '■' && steps != 0)
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx + 2).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx + 2, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx + 2, 0, steps, baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx + 2, 3, steps, baseDirection);
                        }
                    }
                    break;
                case 3:
                    if (map[idx + 2] != '■')
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx + 2).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx + 2, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx + 2, 0, steps,baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx + 2, 3, steps, baseDirection);
                        }
                    }
                    else if (map[idx + 38] != '■' && steps != 0)
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx + 38).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx + 38, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx + 38, 0, steps,baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx + 38, 2, steps, baseDirection);
                        }
                    }
                    else if (map[idx - 38] != '■' && steps != 0)
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx - 38).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx - 38, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx - 38, 0, steps, baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx - 38, 4, steps, baseDirection);
                        }
                    }
                    break;
                case 4:
                    if (map[idx - 38] != '■')
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx - 38).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx - 38, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx - 38, 0, steps, baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx - 38, 4, steps, baseDirection);
                        }
                    }
                    else if (map[idx + 2] != '■' && steps != 0)
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx + 2).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx + 2, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx + 2, 0, steps, baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx + 2, 3, steps, baseDirection);
                        }
                    }
                    else if (map[idx - 2] != '■' && steps != 0)
                    {
                        steps++;
                        if (this.Graph.Where(x => x.ID == idx - 2).Select(x => x).FirstOrDefault() != null)
                        {
                            this.Graph.Where(x => x == item).Select(x => x).FirstOrDefault().Neighbours.Add(idx - 2, new Neighbour(baseDirection, steps));
                            //FindNeighbours(item, idx - 2, 0, steps, baseDirection);
                        }
                        else
                        {
                            FindNeighbours(item, idx - 2, 1, steps, baseDirection);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
