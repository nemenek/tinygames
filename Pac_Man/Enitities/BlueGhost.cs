﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_Man
{
    class BlueGhost : Entity
    {
        private int redGhostPosition;
        public BlueGhost() : base('Ѽ', ConsoleColor.Blue)
        {
            this.Position = 436;
            this.Direction = 0;
            this.food = false;
            this.started = false;
        }

        public void Step(Dictionary<int, char> map, Entity pacman, List<Node> graph, int redGhostPosition)
        {
            OnLogging(new LoggerEvent("Step method called."));
            this.redGhostPosition = redGhostPosition;
            Step(map, pacman, graph);
        }

        public override void Step(Dictionary<int, char> map, Entity pacman, List<Node> graph)
        {
            if (graph.Where(x => x.ID == this.Position).FirstOrDefault() != null)
            {
                Dictionary<Node, int> nodes = new Dictionary<Node, int>();
                List<Node> myGraph = new List<Node>();

                foreach (var node in graph)
                {
                    myGraph.Add(new Node(node.ID)
                    {
                        Distance = node.Distance,
                        Neighbours = new Dictionary<int, Neighbour>(node.Neighbours),
                        Visited = false
                    });
                }

                int goalIdx = 0;

                if (!(pacman.GetPosition() > 446 && pacman.GetPosition() < 455) && !(pacman.GetPosition() < 426 && pacman.GetPosition() > 417)) //Calculate the goal position
                {
                    goalIdx = pacman.GetPosition();
                    int avrg = goalIdx - this.redGhostPosition;
                    goalIdx = (goalIdx + 1) % 38;
                    int helper = (this.redGhostPosition + 1) % 38;
                    int y = 0;
                    int x = 0;

                    if (avrg % 38 == 0)
                    {
                        y = avrg / 38;
                    }
                    else
                    {
                        x = goalIdx - helper;
                        helper = avrg % 38;
                        avrg -= Math.Abs(helper);
                        y = avrg / 38;
                    }
                    if((goalIdx + x) > 38)
                    {
                        x = 35 - goalIdx;
                    }
                    else if((goalIdx + x) < 0)
                    {
                        x = goalIdx - 3;
                    }
                    goalIdx = (pacman.GetPosition() + (y * 38) + x);

                    while (!(goalIdx < map.Count))
                    {
                        goalIdx -= 38;
                    }

                    while (!(goalIdx >= 0))
                    {
                        goalIdx += 38;
                    }

                    if (map[goalIdx] != '*' && map[goalIdx] != ' ')
                    {
                        if ((goalIdx + 1) % 38 == 0)
                        {
                            goalIdx -= 1;
                        }
                        if (goalIdx >= 455)
                        {
                            while (map[goalIdx] != '*' && map[goalIdx] != ' ')
                            {
                                goalIdx -= 38;
                            }
                        }
                        else if (goalIdx < 455)
                        {
                            while (map[goalIdx] != '*' && map[goalIdx] != ' ')
                            {
                                goalIdx += 38;
                            }
                        }
                        if (goalIdx <= 454 && goalIdx > 446)
                        {
                            if(this.Position != 446)
                            {
                                goalIdx = 446;
                            }
                            else
                            {
                                goalIdx = 442;
                            }
                        }
                        else if (goalIdx >= 418 && goalIdx < 426)
                        {
                            if(this.Position != 426)
                            {
                                goalIdx = 426;
                            }
                            else
                            {
                                goalIdx = 430;
                            }
                        }
                    }
                    if (myGraph.Where(k => k.ID == goalIdx).FirstOrDefault() == null)
                    {
                        myGraph.Add(new Node(goalIdx));
                        RefreshNeighbours(myGraph, goalIdx, map);
                    }

                }
                else
                {
                    if (this.redGhostPosition > 455)
                    {
                        goalIdx = 324;
                    }
                    else
                    {
                        goalIdx = 704;
                    }
                }

                OnLogging(new LoggerEvent("Goal index calculated: " + goalIdx));

                myGraph.Where(x => x.ID == this.Position).Select(x => x.Neighbours).FirstOrDefault().Remove(myGraph.Where(x => x.ID == this.Position).Select(x => x.Neighbours).FirstOrDefault().Where(x => x.Value.Direction == this.Direction).Select(x => x.Key).FirstOrDefault());

                nodes.Add(myGraph.Where(x => x.ID == this.Position).FirstOrDefault(), -1);
                nodes.Where(x => x.Key.ID == this.Position).FirstOrDefault().Key.Distance = 0;
                nodes.Where(x => x.Key.ID == this.Position).FirstOrDefault().Key.Visited = true;

                if (goalIdx != this.Position)
                {
                    KeyValuePair<Node, int> element = new KeyValuePair<Node, int>();

                    OnLogging(new LoggerEvent("Dijkstra algorithm called."));
                    Dijkstra(myGraph, ref nodes, this.Position);
                    OnLogging(new LoggerEvent("Dijkstra algorithm finished."));

                    element = (from x in nodes
                               where x.Key.ID == goalIdx
                               select x).FirstOrDefault();

                    while (element.Value != this.Position)
                    {
                        element = nodes.Where(x => x.Key.ID == element.Value).FirstOrDefault();
                    }

                    switch (myGraph.Where(x => x.ID == this.Position).FirstOrDefault().Neighbours.Where(x => x.Key == element.Key.ID).Select(x => x.Value.Direction).FirstOrDefault())
                    {
                        case 1:
                            this.Direction = 3;
                            break;
                        case 2:
                            this.Direction = 4;
                            break;
                        case 3:
                            this.Direction = 1;
                            break;
                        case 4:
                            this.Direction = 2;
                            break;
                    }
                }
            }
            Move(map);
            OnLogging(new LoggerEvent("Step call finished."));
        }
    }
}
