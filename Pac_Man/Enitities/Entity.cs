﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_Man
{
    public delegate void LogEventHandler(LoggerEvent e);
    abstract public class Entity
    {
        protected event LogEventHandler log; //logger event
        public bool started; //represents if the ghost has left the starting box or not
        protected byte Direction { get; set; } //1-right, 2-up, 3-left, 4-down, 0-standing still
        private char toBeDisplayed; //The char displayed on the console
        protected int Position { get; set; } //Entity's position on the map
        abstract public void Step(Dictionary<int, char> map, Entity pacman, List<Node> graph); //Implements the spceial moving algorithm of the entity
        public ConsoleColor Color { get; set; } //The color of the entity

        protected bool food; //Indicated, if the tile the entity stands on still has food or not

        // Constructors
        public Entity(int position, char ToBeDisplayed, ConsoleColor color)
        {
            this.Position = position;
            this.toBeDisplayed = ToBeDisplayed;
            this.Color = color;
            this.log += OnLogEvent;
            File.Delete("logger.log");
        }
        public Entity(char ToBeDisplayed, ConsoleColor color)
        {
            this.toBeDisplayed = ToBeDisplayed;
            this.Color = color;
            this.log += OnLogEvent;
            File.Delete("logger.log");
        }

        // Logger method
        private void OnLogEvent(LoggerEvent e)
        {
            StringBuilder sb = new StringBuilder("[" + e.Time.Year.ToString() + ".");
            sb.Append(e.Time.Month.ToString() + ".");
            sb.Append(e.Time.Day.ToString() + ". ");
            sb.Append(e.Time.Hour.ToString() + ":");
            sb.Append(e.Time.Minute.ToString() + ":");
            sb.Append(e.Time.Second.ToString() + ":");
            sb.Append(e.Time.Millisecond.ToString() + "]");
            sb.Append("\t" + this.Color.ToString() + " entity");
            sb.Append(":\t" + e.Message + "\n");

            File.AppendAllText("logger.log", sb.ToString());

            sb.Clear();
        }

        // descendant classes can use event with this method
        public virtual void OnLogging(LoggerEvent e)
        {
            LogEventHandler handler = this.log;
            if (handler != null)
            {
                handler(e);
            }
        }

        // Returns toBeDisplayed variable
        public char GetDisplay()
        {
            return this.toBeDisplayed;
        }

        // Sets toBeDisplayed variable
        public void SetDisplay(char value)
        {
            if (value != this.toBeDisplayed)
            {
                if (value == 'C' || value == 'Ɔ')
                {
                    this.toBeDisplayed = value;
                }
            }
        }

        // Sets direction property
        public void SetDirection(byte value)
        {
            this.Direction = value;
        }

        // Returns directon property
        public byte GetDirection()
        {
            return this.Direction;
        }

        // Sets started variable and sets entity's new position
        public void SetStarted(bool value, Dictionary<int, char> map)
        {
            if (value)
            {
                map[this.Position] = '-';
                if (map[320] != 'Ѽ')
                {
                    this.Position = 320;
                    if (map[320] == '*')
                    {
                        this.food = true;
                    }
                }
                else if (map[322] != 'Ѽ')
                {
                    this.Position = 322;
                    if(map[322] == '*')
                    {
                        this.food = true;
                    }
                }
                else if(map[324] != 'Ѽ')
                {
                    this.Position = 324;
                    if(map[324] == '*')
                    {
                        this.food = true;
                    }
                }
                else if (map[326] == '*')
                {
                    this.Position = 326;
                    this.food = true;
                }
                else
                {
                    this.Position = 326;
                }
                this.started = value;
            }
        }

        // Returns position property
        public int GetPosition()
        {
            return this.Position;
        }

        // Sets position property
        public void SetPosition(int value)
        {
            this.Position = value;
        }

        // Sets entity's position on the map according to it's direction and position
        public void Move(Dictionary<int, char> map)
        {
            this.log(new LoggerEvent("Move called. Direction: " + this.Direction + "\tPosition: " + this.Position));
            if (this.food)
            {
                map[this.Position] = '*';
            }
            else
            {
                if (this.started)
                {
                    map[this.Position] = ' ';
                }
                else
                {
                    map[this.Position] = '-';
                }
            }
            switch (this.Direction)
            {
                case 1:
                    if (map[this.Position + 2] != '■')
                    {

                        if (map[this.Position + 2] == '*')
                        {
                            this.food = true;
                        }
                        else if(map[this.Position + 2] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position += 2;
                    }
                    else if (map[this.Position + 38] != '■')
                    {
                        if (map[this.Position + 38] == '*')
                        {
                            this.food = true;
                        }
                        else if(map[this.Position + 38] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Direction = 4;
                        this.Position += 38;
                    }
                    else if (map[this.Position - 38] != '■')
                    {
                        if (map[this.Position - 38] == '*')
                        {
                            this.food = true;
                        }
                        else if(map[this.Position - 38] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position -= 38;
                        this.Direction = 2;
                    }
                    break;
                case 2:
                    if (map[this.Position - 38] != '■')
                    {
                        if (map[this.Position - 38] == '*')
                        {
                            this.food = true;
                        }
                        else if(map[this.Position - 38] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position -= 38;
                    }
                    else if (map[this.Position + 2] != '■')
                    {

                        if (map[this.Position + 2] == '*')
                        {
                            this.food = true;
                        }
                        else if(map[this.Position + 2] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position += 2;
                        this.Direction = 1;
                    }
                    else if (map[this.Position - 2] != '■')
                    {
                        if (map[this.Position - 2] == '*')
                        {
                            this.food = true;
                        }
                        else if (map[this.Position - 2] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position -= 2;
                        this.Direction = 3;
                    }
                    break;
                case 3:
                    if (map[this.Position - 2] != '■')
                    {
                        if (map[this.Position - 2] == '*')
                        {
                            this.food = true;
                        }
                        else if (map[this.Position - 2] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position -= 2;
                    }
                    else if (map[this.Position - 38] != '■')
                    {
                        if (map[this.Position - 38] == '*')
                        {
                            this.food = true;
                        }
                        else if(map[this.Position - 38] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position -= 38;
                        this.Direction = 2;
                    }
                    else if (map[this.Position + 38] != '■')
                    {

                        if (map[this.Position + 38] == '*')
                        {
                            this.food = true;
                        }
                        else if(map[this.Position + 38] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position += 38;
                        this.Direction = 4;
                    }

                    break;
                case 4:
                    if (map[this.Position + 38] != '■')
                    {

                        if (map[this.Position + 38] == '*')
                        {
                            this.food = true;
                        }
                        else if(map[this.Position + 38] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position += 38;
                    }
                    else if (map[this.Position - 2] != '■')
                    {
                        if (map[this.Position - 2] == '*')
                        {
                            this.food = true;
                        }
                        else if(map[this.Position - 2] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position -= 2;
                        this.Direction = 3;
                    }
                    else if (map[this.Position + 2] != '■')
                    {
                        if (map[this.Position + 2] == '*')
                        {
                            this.food = true;
                        }
                        else if(map[this.Position + 2] == ' ' || map[this.Position - 38] == 'C' || map[this.Position - 38] == 'Ɔ')
                        {
                            this.food = false;
                        }
                        else
                        {
                            break;
                        }
                        this.Position += 2;
                        this.Direction = 1;
                    }
                    break;
                default:
                    this.Direction = 3;
                    break;
            }
            this.log(new LoggerEvent("Move completed. Direction: " + this.Direction + "\tPosition: " + this.Position));
        }

        // Finds the shortest routes to every node in the given graph from the given node
        public void Dijkstra(List<Node> graph, ref Dictionary<Node, int> nodes, int currentIdx)
        {
            var neighbours = (from x in graph
                              where x.ID == currentIdx
                              orderby x.Neighbours.Values ascending
                              select x.Neighbours).FirstOrDefault();

            bool[] callMore = new bool[neighbours.Count];

            for (int i = 0; i < callMore.Length; i++)
            {
                callMore[i] = false;
            }

            if (neighbours != null)
            {
                int i = 0;
                foreach (var item in neighbours)
                {
                    if (!graph.Where(x => x.ID == item.Key).Select(x => x).FirstOrDefault().Visited)
                    {
                        nodes.Add(graph.Where(x => x.ID == item.Key).Select(x => x).FirstOrDefault(), currentIdx);
                        nodes.Where(x => x.Key.ID == item.Key).Select(x => x).FirstOrDefault().Key.Distance = nodes.Where(x => x.Key.ID == currentIdx).Select(x => x.Key.Distance).FirstOrDefault() + item.Value.Distance;
                        nodes.Where(x => x.Key.ID == item.Key).Select(x => x).FirstOrDefault().Key.Visited = true;
                        callMore[i] = true;
                    }
                    else if (nodes.Where(x => x.Key.ID == item.Key).Select(x => x).FirstOrDefault().Key.Distance > nodes.Where(x => x.Key.ID == currentIdx).Select(x => x.Key.Distance).FirstOrDefault() + item.Value.Distance)
                    {
                        nodes[nodes.Where(x => x.Key.ID == item.Key).Select(x => x).FirstOrDefault().Key] = currentIdx;
                        nodes.Where(x => x.Key.ID == item.Key).Select(x => x).FirstOrDefault().Key.Distance = nodes.Where(x => x.Key.ID == currentIdx).Select(x => x.Key.Distance).FirstOrDefault() + item.Value.Distance;
                        callMore[i] = true;
                    }
                    i++;
                }
                i = 0;
                foreach (var item in neighbours)
                {
                    if (callMore[i])
                    {
                        Dijkstra(graph, ref nodes, item.Key);
                    }
                    i++;
                }
            }
        }

        // Refreshes the given graph's given node's neighbours
        public void RefreshNeighbours(List<Node> graph, int sourceIdx, Dictionary<int, char> map)
        {
            for (int i = 1; i < 5; i++)
            {
                RefreshNeighbours(i, sourceIdx, Convert.ToByte(i), 0, sourceIdx, map, graph);
            }

            RefreshNeighboursVol2(sourceIdx, graph);
        }
        // Essential for RefreshNeighbours method
        private void RefreshNeighbours(int direction, int idx, byte originalDirection, int step, int sourceIdx, Dictionary<int, char> map, List<Node> graph)
        {
            switch (direction)
            {
                case 1:
                    if (map[idx - 2] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx - 2).Select(x => x).FirstOrDefault() != null)
                        {
                            graph.Where(x => x.ID == idx - 2).Select(x => x.Neighbours).FirstOrDefault().Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx - 2, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(1, idx - 2, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    else if (step != 0 && map[idx - 38] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx - 38).Select(x => x).FirstOrDefault() != null)
                        {
                            direction = 4;
                            step++;
                            if (graph.Where(x => x.ID == idx - 38).Select(x => x).FirstOrDefault() != null)
                            {
                                var node = graph.Where(x => x.ID == idx - 38).Select(x => x).FirstOrDefault();      //cannot comment on this section either, magic happens or idk

                                node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                                graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx - 38, new Neighbour(originalDirection, step));
                            }
                        }
                        else
                        {
                            RefreshNeighbours(4, idx - 38, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    else if (step != 0 && map[idx + 38] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx + 38).Select(x => x).FirstOrDefault() != null)
                        {
                            direction = 2;

                            var node = graph.Where(x => x.ID == idx + 38).Select(x => x).FirstOrDefault();      //cannot comment on this section either, magic happens or idk

                            node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx + 38, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(2, idx + 38, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    break;
                case 2:
                    if (map[idx + 38] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx + 38).Select(x => x).FirstOrDefault() != null)
                        {

                            var node = graph.Where(x => x.ID == idx + 38).Select(x => x).FirstOrDefault();      //cannot comment on this section either, magic happens or idk

                            node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx + 38, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(2, idx + 38, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    else if (step != 0 && map[idx - 2] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx - 2).Select(x => x).FirstOrDefault() != null)
                        {
                            direction = 1;

                            var node = graph.Where(x => x.ID == idx - 2).Select(x => x).FirstOrDefault();      //cannot comment on this section either, magic happens or idk

                            node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx - 2, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(1, idx - 2, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    else if (step != 0 && map[idx + 2] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx + 2).Select(x => x).FirstOrDefault() != null)
                        {
                            direction = 3;

                            var node = graph.Where(x => x.ID == idx + 2).Select(x => x).FirstOrDefault();      //cannot comment on this section either, magic happens or idk

                            node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx + 2, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(3, idx + 2, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    break;
                case 3:
                    if (map[idx + 2] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx + 2).FirstOrDefault() != null)
                        {

                            var node = graph.Where(x => x.ID == idx + 2).FirstOrDefault();      //cannot comment on this section either, magic happens or idk


                            node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx + 2, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(3, idx + 2, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    else if (step != 0 && map[idx + 38] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx + 38).FirstOrDefault() != null)
                        {
                            direction = 2;

                            var node = graph.Where(x => x.ID == idx + 38).FirstOrDefault();      //cannot comment on this section either, magic happens or idk
                            node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx + 38, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(2, idx + 38, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    else if (step != 0 && map[idx - 38] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx - 38).Select(x => x).FirstOrDefault() != null)
                        {
                            direction = 4;

                            var node = graph.Where(x => x.ID == idx - 38).Select(x => x).FirstOrDefault();      //cannot comment on this section either, magic happens or idk
                            node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx - 38, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(4, idx - 38, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    break;
                case 4:
                    if (map[idx - 38] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx - 38).Select(x => x).FirstOrDefault() != null)
                        {

                            var node = graph.Where(x => x.ID == idx - 38).Select(x => x).FirstOrDefault();      //cannot comment on this section either, magic happens or idk
                            node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx - 38, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(4, idx - 38, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    else if (step != 0 && map[idx + 2] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx + 2).Select(x => x).FirstOrDefault() != null)
                        {
                            direction = 3;

                            var node = graph.Where(x => x.ID == idx + 2).Select(x => x).FirstOrDefault();      //cannot comment on this section either, magic happens or idk
                            node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx + 2, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(3, idx + 2, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    else if (step != 0 && map[idx - 2] != '■')
                    {
                        step++;
                        if (graph.Where(x => x.ID == idx - 2).Select(x => x).FirstOrDefault() != null)
                        {
                            direction = 1;

                            var node = graph.Where(x => x.ID == idx - 2).Select(x => x).FirstOrDefault();      //cannot comment on this section either, magic happens or idk
                            node.Neighbours.Add(sourceIdx, new Neighbour(direction < 3 ? Convert.ToByte(direction + 2) : Convert.ToByte(direction - 2), step));
                            graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault().Add(idx - 2, new Neighbour(originalDirection, step));

                        }
                        else
                        {
                            RefreshNeighbours(1, idx - 2, originalDirection, step, sourceIdx, map, graph);
                        }
                    }
                    break;
            }
        }
        // Essential for RefreshNeighbours method
        private void RefreshNeighboursVol2(int sourceIdx, List<Node> graph)
        {
            var neigbours = graph.Where(x => x.ID == sourceIdx).Select(x => x.Neighbours).FirstOrDefault();

            List<Node> nodes = new List<Node>();

            foreach (var item in neigbours)
            {
                nodes.Add(graph.Where(x => x.ID == item.Key).Select(x => x).FirstOrDefault());
            }

            var ids = (from x in neigbours
                       select x.Key).ToList();

            for (int i = 0; i < ids.Count; i++)
            {
                if (nodes[0].Neighbours.ContainsKey(ids[i]))
                {
                    nodes[0].Neighbours.Remove(ids[i]);
                }
                else if (nodes.Count > 1 && nodes[1].Neighbours.ContainsKey(ids[i]))
                {
                    nodes[1].Neighbours.Remove(ids[i]);
                }
            }
        }
    }
}
