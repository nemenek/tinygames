﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_Man
{
    class PacMan : Entity
    {
        public PacMan(int position, char toBeDisplayed, ConsoleColor color) : base(position, toBeDisplayed, color)
        {

        }

        public PacMan() : base(702, 'C', ConsoleColor.Yellow) //Ɔ
        {
            this.Direction = 0;
        }

        public override void Step(Dictionary<int, char> map, Entity pacman, List<Node> graph)
        {
            OnLogging(new LoggerEvent("Step method called. Position: " + this.Position));
            switch (this.Direction) 
            {
                case 0:

                    break;
                case 1:
                    map[this.Position] = ' ';
                    this.Position += 2;
                    break;
                case 2:
                    map[this.Position] = ' ';
                    this.Position -= 38;
                    break;
                case 3:
                    map[this.Position] = ' ';
                    this.Position -= 2;
                    break;
                case 4:
                    map[this.Position] = ' ';
                    this.Position += 38;
                    break;
            }
            OnLogging(new LoggerEvent("Step method finished. Position: " + this.Position));
        }

    }
}
