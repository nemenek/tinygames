﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_Man
{
    class OrangeGhost : Entity
    {
        public OrangeGhost() : base ('Ѽ', ConsoleColor.DarkYellow)
        {
            this.Position = 434;
            this.food = false;
            this.started = false;
        }

        public override void Step(Dictionary<int, char> map, Entity pacman, List<Node> graph)
        {
            OnLogging(new LoggerEvent("Step method called."));
            if (graph.Where(x=>x.ID == this.Position).FirstOrDefault() != null)
            {
                Dictionary<Node, int> nodes = new Dictionary<Node, int>();
                List<Node> myGraph = new List<Node>();

                foreach (var node in graph)
                {
                    myGraph.Add(new Node(node.ID)
                    {
                        Distance = node.Distance,
                        Neighbours = new Dictionary<int, Neighbour>(node.Neighbours),
                        Visited = false
                    });
                }

                if (myGraph.Where(x => x.ID == pacman.GetPosition()).FirstOrDefault() == null)
                {
                    myGraph.Add(new Node(pacman.GetPosition()));
                    RefreshNeighbours(myGraph, pacman.GetPosition(), map);
                }

                myGraph.Where(x => x.ID == this.Position).Select(x => x.Neighbours).FirstOrDefault().Remove(myGraph.Where(x => x.ID == this.Position).Select(x => x.Neighbours).FirstOrDefault().Where(x => x.Value.Direction == this.Direction).Select(x => x.Key).FirstOrDefault());

                nodes.Add(myGraph.Where(x => x.ID == this.Position).Select(x => x).FirstOrDefault(), -1);
                nodes.Where(x => x.Key.ID == this.Position).Select(x => x).FirstOrDefault().Key.Distance = 0;
                nodes.Where(x => x.Key.ID == this.Position).Select(x => x).FirstOrDefault().Key.Visited = true;

                KeyValuePair<Node, int> element = new KeyValuePair<Node, int>();

               OnLogging(new LoggerEvent("Dijkstra algorithm called."));
                Dijkstra(myGraph, ref nodes, this.Position);
                OnLogging(new LoggerEvent("Dijkstra algorithm finished."));

                if (!(pacman.GetPosition() >= 446 && pacman.GetPosition() < 455) && !(pacman.GetPosition() <= 426 && pacman.GetPosition() > 417))
                {
                    element = (from x in nodes
                               where x.Key.ID == pacman.GetPosition()
                               select x).FirstOrDefault();
                }
                else
                {
                    if (this.Position == 446)
                    {
                        element = (from x in nodes
                                   where x.Key.ID == 426
                                   select x).FirstOrDefault();
                    }
                    else
                    {
                        element = (from x in nodes
                                   where x.Key.ID == 446
                                   select x).FirstOrDefault();
                    }
                }

                int distance = element.Key.Distance;

                if(distance > 8)
                {
                    while (element.Value != this.Position)
                    {
                        distance += element.Key.Distance;
                        element = nodes.Where(x => x.Key.ID == element.Value).FirstOrDefault();
                    }

                    switch (myGraph.Where(x => x.ID == this.Position).FirstOrDefault().Neighbours.Where(x => x.Key == element.Key.ID).Select(x => x.Value.Direction).FirstOrDefault())
                    {
                        case 1:
                            this.Direction = 3;
                            break;
                        case 2:
                            this.Direction = 4;
                            break;
                        case 3:
                            this.Direction = 1;
                            break;
                        case 4:
                            this.Direction = 2;
                            break;
                    }
                }
                else if(this.Position == 426 && this.Direction == 3)
                {
                    this.Direction = 2;
                }
                else if(this.Position == 446 && this.Direction == 1)
                {
                    this.Direction = 4;
                }
                
            }
            Move(map);
            OnLogging(new LoggerEvent("Step method finished."));
        }
    }
}
