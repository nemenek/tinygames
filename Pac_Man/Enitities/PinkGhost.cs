﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_Man
{
    class PinkGhost : Entity
    {
        public PinkGhost() : base('Ѽ', ConsoleColor.Magenta)
        {
            this.Position = 438;
            this.food = false;
            this.Direction = 4;
            this.started = false;
        }

        private int FindGoalIndex(Dictionary<int, char> map, int direction, int step, int position)
        {
            switch (direction)
            {
                case 1:
                    if (map[position + 2] != '■')
                    {
                        step++;
                        position += 2;
                    }
                    else if (map[position + 38] != '■')
                    {
                        step++;
                        direction = 4;
                        position += 38;
                    }
                    else if (map[position - 38] != '■')
                    {
                        step++;
                        direction = 2;
                        position -= 38;
                    }
                    break;
                case 2:
                    if (map[position - 38] != '■')
                    {
                        step++;
                        position -= 38;
                    }
                    else if (map[position + 2] != '■')
                    {
                        step++;
                        direction = 1;
                        position += 2;
                    }
                    else if (map[position - 2] != '■')
                    {
                        step++;
                        direction = 3;
                        position -= 2;
                    }
                    break;
                case 3:
                    if (map[position - 2] != '■')
                    {
                        step++;
                        position -= 2;
                    }
                    else if (map[position - 38] != '■')
                    {
                        step++;
                        direction = 2;
                        position -= 38;
                    }
                    else if (map[position + 38] != '■')
                    {
                        step++;
                        direction = 3;
                        position += 38;
                    }
                    break;
                case 4:
                    if (map[position + 38] != '■')
                    {
                        step++;
                        position += 38;
                    }
                    else if (map[position - 2] != '■')
                    {
                        step++;
                        direction = 3;
                        position -= 2;
                    }
                    else if (map[position + 2] != '■')
                    {
                        step++;
                        direction = 1;
                        position += 2;
                    }
                    break;
                case 0:
                    if (map[position + 2] != '■')
                    {
                        step++;
                        position += 2;
                    }
                    else if (map[position + 38] != '■')
                    {
                        step++;
                        direction = 4;
                        position += 38;
                    }
                    else if (map[position - 38] != '■')
                    {
                        step++;
                        direction = 2;
                        position -= 38;
                    }
                    else
                    {
                        step++;
                        direction = 3;
                        position -= 2;
                    }
                    break;
            }
            if (step == 4)
            {
                return position;
            }
            else
            {
                return FindGoalIndex(map, direction, step, position);
            }
        }

        public override void Step(Dictionary<int, char> map, Entity pacman, List<Node> graph)
        {
            OnLogging(new LoggerEvent("Step method called."));
            if (graph.Where(x => x.ID == this.Position).Select(x => x).FirstOrDefault() != null)
            {
                Dictionary<Node, int> nodes = new Dictionary<Node, int>();
                List<Node> myGraph = new List<Node>();

                foreach (var node in graph)
                {
                    myGraph.Add(new Node(node.ID)
                    {
                        Distance = node.Distance,
                        Neighbours = new Dictionary<int, Neighbour>(node.Neighbours),
                        Visited = false
                    });
                }

                int goal = 0;

                if (!(pacman.GetPosition() >= 446 && pacman.GetPosition() < 455) && !(pacman.GetPosition() <= 426 && pacman.GetPosition() > 417))
                {
                    goal = FindGoalIndex(map, pacman.GetDirection(), 0, pacman.GetPosition());
                }
                else
                {
                    goal = 322;
                }
                OnLogging(new LoggerEvent("Goal index calculated: " + goal));

                if (myGraph.Where(x => x.ID == goal).FirstOrDefault() == null)
                {
                    myGraph.Add(new Node(goal));
                    RefreshNeighbours(myGraph, goal, map);
                }

                myGraph.Where(x => x.ID == this.Position).Select(x => x.Neighbours).FirstOrDefault().Remove(myGraph.Where(x => x.ID == this.Position).Select(x => x.Neighbours).FirstOrDefault().Where(x => x.Value.Direction == this.Direction).Select(x => x.Key).FirstOrDefault());

                nodes.Add(myGraph.Where(x => x.ID == this.Position).FirstOrDefault(), -1);
                nodes.Where(x => x.Key.ID == this.Position).FirstOrDefault().Key.Distance = 0;
                nodes.Where(x => x.Key.ID == this.Position).FirstOrDefault().Key.Visited = true;
                if (goal != this.Position)
                {
                    KeyValuePair<Node, int> element = new KeyValuePair<Node, int>();

                    OnLogging(new LoggerEvent("Dijkstra algorithm called."));
                    Dijkstra(myGraph, ref nodes, this.Position);
                    OnLogging(new LoggerEvent("Dijkstra algorithm finished."));

                    element = (from x in nodes
                               where x.Key.ID == goal
                               select x).FirstOrDefault();

                    while (element.Value != this.Position)
                    {
                        element = nodes.Where(x => x.Key.ID == element.Value).FirstOrDefault();
                    }

                    switch (myGraph.Where(x => x.ID == this.Position).FirstOrDefault().Neighbours.Where(x => x.Key == element.Key.ID).Select(x => x.Value.Direction).FirstOrDefault())
                    {
                        case 1:
                            this.Direction = 3;
                            break;
                        case 2:
                            this.Direction = 4;
                            break;
                        case 3:
                            this.Direction = 1;
                            break;
                        case 4:
                            this.Direction = 2;
                            break;
                    }
                }
                else if (this.Position == 426 && this.Direction == 3)
                {
                    this.Direction = 2;
                }
                else if (this.Position == 446 && this.Direction == 1)
                {
                    this.Direction = 4;
                }
            }
            Move(map);
            OnLogging(new LoggerEvent("Step method finished."));
        }
    }
}
