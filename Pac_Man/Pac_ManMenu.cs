﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_Man
{
    public class Pac_ManMenu
    {
        public Pac_ManMenu()
        {
            bool exit = false;
            do
            {
                Console.Clear();
                Console.WriteLine("\tPAC MAN\n");
                Console.WriteLine("Press Enter To Start Game!\nEsc - Main Menu");
                var key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.Enter:
                        Game game = new Game();
                        break;
                    case ConsoleKey.Escape:
                        exit = true;
                        break;
                }
            } while (!exit);
        }
    }
}
