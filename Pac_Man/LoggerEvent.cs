﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_Man
{
    public class LoggerEvent : EventArgs
    {
        public string Message { get; }
        public DateTime Time { get; }
        public LoggerEvent(string message)
        {
            this.Message = message;
            this.Time = DateTime.Now;
        }
    }
}
