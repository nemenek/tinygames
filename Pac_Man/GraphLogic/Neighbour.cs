﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_Man
{
    public class Neighbour
    {
        public byte Direction { get; set; }
        public int Distance { get; set; }
        public Neighbour(byte direction, int distance)
        {
            this.Direction = direction;
            this.Distance = distance;
        }
    }
}
