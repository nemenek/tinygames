﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pac_Man
{
    public class Node
    {
        public bool Visited { get; set; }
        public int ID { get; set; }
        public Dictionary<int,Neighbour> Neighbours { get; set; }
        public int Distance { get; set; }
        public Node(int id)
        {
            this.ID = id;
            this.Distance = 0;
            this.Visited = false;
            this.Neighbours = new Dictionary<int, Neighbour>();
        }
    }
}
