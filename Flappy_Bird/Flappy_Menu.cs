﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Flappy_Bird
{
    public class Flappy_Menu
    {
        private bool lvl2;
        private bool lvl3;
        private bool lvl4;
        private bool impossible;

        private Game game;
        public Flappy_Menu()
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();
            SetLevels();
            WriteMenu();
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }

        private void WriteMenu()
        {
            Console.SetCursorPosition(40, 7);
            Console.Write("Welcome to");
            Console.SetCursorPosition(40, 8);
            Console.WriteLine("Flappy Bird!");
            Console.ReadKey();
            Console.Clear();
        ChooseMode:
            Console.WriteLine("Choose Game Mode!");
            Console.WriteLine("1. Levels");
            Console.WriteLine("2. Arcade");
            Console.WriteLine("\nEsc - Cancel");
            var choice = Console.ReadKey(true);
            switch (choice.Key)
            {
                case ConsoleKey.NumPad1: case ConsoleKey.D1:
                    while (true)
                    {
                        Console.Clear();
                        Console.WriteLine("There are 4 Levels,\nEvery next level will be unlocked once you completed the previous one.");
                        Console.WriteLine("1. - Level 1");
                        if (this.lvl2)
                        {
                            Console.WriteLine("2. - Level 2");
                            if (this.lvl3)
                            {
                                Console.WriteLine("3. - Level 3");
                                if (this.lvl4)
                                {
                                    Console.WriteLine("4. - Level 4");
                                }
                            }
                        }
                        Console.WriteLine("\nEsc - Cancel");
                        choice = Console.ReadKey(true);
                        switch (choice.Key)
                        {
                            case ConsoleKey.D1:
                            case ConsoleKey.NumPad1:
                                this.game = new Game(GameMode.Lvl1);
                                if(game.completed)
                                {
                                    this.lvl2 = true;
                                }
                                Console.SetWindowSize(120, 30);
                                break;
                            case ConsoleKey.D2:
                            case ConsoleKey.NumPad2:
                                if (this.lvl2)
                                {
                                    this.game = new Game(GameMode.Lvl2);
                                    if (game.completed)
                                    {
                                        this.lvl3 = true;
                                    }
                                    Console.SetWindowSize(120, 30);
                                }
                                break;
                            case ConsoleKey.D3:
                            case ConsoleKey.NumPad3:
                                if (this.lvl3)
                                {
                                    this.game = new Game(GameMode.Lvl3);
                                    if (this.game.completed)
                                    {
                                        this.lvl4 = true;
                                    }
                                    Console.SetWindowSize(120, 30);
                                }
                                break;
                            case ConsoleKey.D4: case ConsoleKey.NumPad4:
                                if (this.lvl4)
                                {
                                    this.game = new Game(GameMode.Lvl4);
                                    Console.SetWindowSize(120, 30);
                                    if (this.game.completed)
                                    {
                                        this.impossible = true;
                                    }
                                }
                                break;
                            case ConsoleKey.Escape:
                                Console.Clear();
                                goto ChooseMode;
                                
                            default:
                                break;
                        }
                    }

                case ConsoleKey.D2: case ConsoleKey.NumPad2:
                    ConsoleKeyInfo difficulty;
                    while (true)
                    {
                        Console.Clear();
                        Console.WriteLine("Please choose Difficulty!");
                        Console.WriteLine("1. Easy");
                        Console.WriteLine("2. Normal");
                        Console.WriteLine("3. Hard");
                        if (this.impossible)
                        {
                            Console.WriteLine("4. Impossible");
                        }
                        
                        Console.WriteLine("\nEsc - Back");
                        difficulty = Console.ReadKey(true);
                        switch (difficulty.Key)
                        {
                            case ConsoleKey.D1:
                            case ConsoleKey.NumPad1:
                                this.game = new Game(GameMode.Easy);
                                Console.SetWindowSize(120, 30);
                                break;
                            case ConsoleKey.D2:
                            case ConsoleKey.NumPad2:
                                this.game = new Game(GameMode.Normal);
                                Console.SetWindowSize(120, 30);
                                break;
                            case ConsoleKey.D3:
                            case ConsoleKey.NumPad3:
                                this.game = new Game(GameMode.Hard);
                                Console.SetWindowSize(120, 30);
                                break;
                            case ConsoleKey.D4:
                            case ConsoleKey.NumPad4:
                                if (this.impossible)
                                {
                                    this.game = new Game(GameMode.Impossible);
                                    Console.SetWindowSize(120, 30);
                                }
                                break;
                            case ConsoleKey.Escape:
                                Console.Clear();
                                goto ChooseMode;
                            default:

                                break;
                        }
                    }
            }
        }

        private void SetLevels()
        {
            if (File.Exists("record.xml"))
            {
                XDocument xdoc = XDocument.Load("record.xml");
                string myBool = (from x in xdoc.Descendants("Record")
                                 where x.Element("GameMode").Value == "Level2"
                                 select x.Element("Completed")).FirstOrDefault().Value;
                if (myBool == "true")
                {
                    this.lvl3 = true;
                }
                else
                {
                    this.lvl3 = false;
                }
                myBool = (from x in xdoc.Descendants("Record")
                          where x.Element("GameMode").Value == "Level3"
                          select x.Element("Completed")).FirstOrDefault().Value;
                if (myBool == "true")
                {
                    this.lvl4 = true;
                }
                else
                {
                    this.lvl4 = false;
                }

                myBool = (from x in xdoc.Descendants("Record")
                          where x.Element("GameMode").Value == "Level4"
                          select x.Element("Completed")).FirstOrDefault().Value;

                if (myBool == "true")
                {
                    this.impossible = true;
                }
                else
                {
                    this.impossible = false;
                }

                myBool = (from x in xdoc.Descendants("Record")
                          where x.Element("GameMode").Value == "Level1"
                          select x.Element("Completed")).FirstOrDefault().Value;

                if (myBool == "true")
                {
                    this.lvl2 = true;
                }
                else
                {
                    this.lvl2 = false;
                }
            }
            else
            {
                this.lvl2 = false;
                this.lvl3 = false;
                this.lvl4 = false;
            }
        }
    }
}
