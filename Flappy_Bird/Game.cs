﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Linq;

namespace Flappy_Bird
{
    enum GameMode
    {
        Lvl1, Lvl2, Lvl3, Lvl4, Easy, Normal, Hard, Impossible
    }
    class Game
    {
        public bool completed;
        private bool newRecord;
        private bool isOver;
        private bool exit;

        private List<string> pitch;
        private List<Obstacle> obs;
        private Bird bird;

        private int Size;
        private int Steps;
        private int PreviousHighScore;
        private int Height;
        private int speed;
        private int goal;

        private ConsoleKeyInfo key;

        XDocument xdoc;

        GameMode gamemode;

        public int Point { get; private set; }

        public Game(GameMode gamemode)
        {
            this.gamemode = gamemode;
            this.PreviousHighScore = GetPreviousHighScore();
            switch (gamemode) // Set game parameters according to game mode
            {
                case GameMode.Easy:
                    this.Size = 60;
                    this.Height = 27;
                    this.speed = 90;
                    this.goal = -1;
                    Console.WriteLine("High Score: " + this.PreviousHighScore + "\nUse SPACE to jump with the bird\nPress any key to start...");
                    break;
                case GameMode.Lvl1:
                    this.Size = 60;
                    this.Height = 27;
                    this.speed = 90;
                    this.goal = 20;
                    Console.WriteLine("Press any key to start...");
                    break;
                case GameMode.Normal:
                    this.Size = 80;
                    this.Height = 20;
                    this.speed = 90;
                    this.goal = -1;
                    Console.WriteLine("High Score: " + this.PreviousHighScore + "\nUse SPACE to jump with the bird\nPress any key to start...");
                    break;
                case GameMode.Lvl2:
                    this.Size = 80;
                    this.Height = 20;
                    this.speed = 80;
                    this.goal = 25;
                    Console.WriteLine("Press any key to start...");
                    break;
                case GameMode.Hard:
                    this.Size = 80;
                    this.Height = 16;
                    this.speed = 90;
                    this.goal = -1;
                    Console.WriteLine("High Score: " + this.PreviousHighScore + "\nUse SPACE to jump with the bird\nPress any key to start...");
                    break;
                case GameMode.Lvl3:
                    this.Size = 80;
                    this.Height = 16;
                    this.speed = 80;
                    this.goal = 30;
                    Console.WriteLine("Press any key to start...");
                    break;
                case GameMode.Lvl4:
                    this.Size = 80;
                    this.Height = 16;
                    this.speed = 60;
                    this.goal = 30;
                    Console.WriteLine("Press any key to start...");
                    break;
                case GameMode.Impossible:
                    this.Size = 100;
                    this.Height = 8;
                    this.speed = 90;
                    this.goal = -1;
                    Console.WriteLine("High Score: " + this.PreviousHighScore + "\nUse SPACE to jump with the bird\nPress any key to start...");
                    break;
                default:
                    break;
            }
            Console.ReadKey(true);
            Console.Clear();
            this.exit = false;
            this.newRecord = false;
            this.Point = 0;
            this.Steps = 0;
            this.isOver = false;
            this.pitch = new List<string>();
            Console.SetWindowSize(this.Size + 2, this.Height + 9);
            do
            {
                switch (this.gamemode)
                {
                    case GameMode.Easy:
                    case GameMode.Lvl1:
                    case GameMode.Normal:
                    case GameMode.Hard:
                    case GameMode.Impossible:
                        this.speed = 90;
                        break;
                    case GameMode.Lvl2:
                    case GameMode.Lvl3:
                        this.speed = 80;
                        break;
                    case GameMode.Lvl4:
                        this.speed = 60;
                        break;
                    default:
                        break;
                }
                pitch.Clear();
                this.isOver = false;
                this.bird = new Bird(this.Height);
                this.obs = new List<Obstacle>();
                Console.Clear();
                for (int i = 3; i > 0; i--)
                {
                    Console.SetCursorPosition(30, 10);
                    Console.WriteLine("Starting in " + i);
                    Thread.Sleep(750);
                }
                Console.Clear();
                while (Console.KeyAvailable)
                {
                    Console.ReadKey(true);
                }
                InitializePitch();
                Display();

                do
                {
                    if ((this.Steps % 125) == 0 && speed != 50)
                    {
                        speed -= 5;
                    }
                    Thread.Sleep(speed);
                    pitch.Clear();
                    ScanKeyBoard();
                    OneStep();
                    InitializePitch();
                    Display();
                    CheckBirdLifeSigns();
                } while (!isOver);
                Console.SetCursorPosition(0, this.Height + 2);
                if (this.gamemode == GameMode.Easy || this.gamemode == GameMode.Normal || this.gamemode == GameMode.Hard || this.gamemode == GameMode.Impossible)
                {
                    if (this.Point > this.PreviousHighScore)
                    {
                        this.PreviousHighScore = this.Point;
                        this.newRecord = true;
                        Console.WriteLine("New Record!\n");
                    }
                    Console.WriteLine("Game Over");
                    Console.WriteLine("Flappy Bird\nHigh Score: " + this.PreviousHighScore + "\nYour point: " + this.Point + "\nPress enter to play again, press escape to exit");
                }
                else
                {
                    if (this.completed)
                    {
                        Console.WriteLine("Congratulations!\nYou completed this level!");
                        if (this.gamemode != GameMode.Lvl4)
                        {
                            Console.WriteLine("Next level is unlocked.");
                        }
                        else
                        {
                            Console.WriteLine("Congratulations!");
                            Console.WriteLine("You have unlocked impossible arcade mode!");
                        }
                        this.exit = true;
                        Console.WriteLine("Press any key to continue...");
                        if (this.xdoc == null)
                        {
                            this.xdoc = XDocument.Load("record.xml");
                        }
                        XElement element = null;
                        switch (this.gamemode)
                        {
                            case GameMode.Lvl1:
                                element = (from x in xdoc.Descendants("Record")
                                           where x.Element("GameMode").Value == "Level1"
                                           select x.Element("Completed")).FirstOrDefault();
                                break;
                            case GameMode.Lvl2:
                                element = (from x in xdoc.Descendants("Record")
                                           where x.Element("GameMode").Value == "Level2"
                                           select x.Element("Completed")).FirstOrDefault();
                                break;
                            case GameMode.Lvl3:
                                element = (from x in xdoc.Descendants("Record")
                                           where x.Element("GameMode").Value == "Level3"
                                           select x.Element("Completed")).FirstOrDefault();
                                break;
                            case GameMode.Lvl4:
                                element = (from x in xdoc.Descendants("Record")
                                           where x.Element("GameMode").Value == "Level4"
                                           select x.Element("Completed")).FirstOrDefault();
                                break;
                        }
                        element.Value = "true";
                        xdoc.Save("record.xml");
                    }
                    else
                    {
                        Console.WriteLine("Level Failed");
                        Console.WriteLine("Your point:" + this.Point);
                        Console.WriteLine("Goal: " + this.goal);
                        Console.WriteLine("Press enter to try again,");
                        Console.WriteLine("Press escape to exit.");
                    }

                }
                var key = Console.ReadKey(true).Key;
                this.Steps = 0;
                this.Point = 0;
                while (key == ConsoleKey.Spacebar)
                {
                    Console.WriteLine("(Except Space)");
                    key = Console.ReadKey(true).Key;
                }
                if (key == ConsoleKey.Escape)
                {
                    exit = true;
                }
                if (this.newRecord)
                {
                    if (this.xdoc == null)
                    {
                        this.xdoc = XDocument.Load("record.xml");
                    }
                    XElement element = null;
                    switch (this.gamemode)
                    {
                        case GameMode.Easy:
                            element = (from x in xdoc.Descendants("Record")
                                       where x.Element("GameMode").Value == "Easy"
                                       select x.Element("HighScore")).FirstOrDefault();
                            break;
                        case GameMode.Normal:
                            element = (from x in xdoc.Descendants("Record")
                                       where x.Element("GameMode").Value == "Normal"
                                       select x.Element("HighScore")).FirstOrDefault();
                            break;
                        case GameMode.Hard:
                            element = (from x in xdoc.Descendants("Record")
                                       where x.Element("GameMode").Value == "Hard"
                                       select x.Element("HighScore")).FirstOrDefault();
                            break;
                        case GameMode.Impossible:
                            element = (from x in xdoc.Descendants("Record")
                                       where x.Element("GameMode").Value == "Impossible"
                                       select x.Element("HighScore")).FirstOrDefault();
                            break;
                    }
                    element.Value = this.PreviousHighScore.ToString();
                    xdoc.Save("record.xml");
                }
            } while (!this.exit);
        }

        private void InsertRow(int rowIdx)
        {
            pitch.Add("*");

            var obsIdx = (from x in obs
                          select x.Position).ToList();

            bool needSpace = true;

            for (int i = 0; i < this.Size; i++)
            {
                if (obsIdx.Contains(i))
                {
                    var ob = (from x in obs
                              where x.Position == i
                              select x).FirstOrDefault();
                    if (ob.GetDirection() == 1)
                    {
                        if (rowIdx >= (this.Height - ob.GetLength()))
                        {
                            if (rowIdx == (this.Height - ob.GetLength()))
                            {
                                pitch.Add("*");
                                pitch.Add("*");
                                pitch.Add("*");
                                pitch.Add("*");
                                if (i == 76)
                                {
                                    needSpace = false;
                                }
                                i += 4;
                            }
                            else
                            {
                                pitch.Add("*");
                                pitch.Add(" ");
                                pitch.Add(" ");
                                pitch.Add("*");
                                if (i == 76)
                                {
                                    needSpace = false;
                                }
                                i += 4;
                            }
                        }
                    }
                    else
                    {
                        if (rowIdx <= (ob.GetLength() - 1))
                        {
                            if (rowIdx == (ob.GetLength() - 1))
                            {
                                pitch.Add("*");
                                pitch.Add("*");
                                pitch.Add("*");
                                pitch.Add("*");
                                if (i == 76)
                                {
                                    needSpace = false;
                                }
                                i += 4;
                            }
                            else
                            {
                                pitch.Add("*");
                                pitch.Add(" ");
                                pitch.Add(" ");
                                pitch.Add("*");
                                if (i == 76)
                                {
                                    needSpace = false;
                                }
                                i += 4;
                            }
                        }
                    }
                }
                if (i == 4 && rowIdx == this.bird.GetPosition())
                {
                    pitch.Add(this.bird.ToString());
                }
                else if (needSpace)
                {
                    pitch.Add(" ");
                }
                else
                {
                    needSpace = true;
                }
            }

            pitch.Add("*");
            pitch.Add("\n");
        }

        private List<string> InitializePitch()
        {
            for (int i = 0; i < this.Size + 2; i++)
            {
                pitch.Add("*");
            }

            pitch.Add("\n");

            for (int i = 0; i < this.Height; i++)
            {
                InsertRow(i);
            }

            for (int i = 0; i < this.Size + 2; i++)
            {
                pitch.Add("*");
            }
            pitch.Add("\nPoint: " + this.Point);
            if (this.goal != -1)
            {
                pitch.Add("\nGoal: " + this.goal);
            }
            return pitch;
        }

        private void Display()
        {
            string s = "";
            foreach (string item in this.pitch)
            {
                s += item;
            }
            Console.SetCursorPosition(0, 0);
            Console.Write(s);
        }

        private void OneStep()
        {
            if (this.bird.GetPosition() != this.Height - 1)
            {
                this.bird.SetPosition(CalculateBirdPosition());
                if (this.bird.Speed < 0.0)
                {
                    if (this.bird.Speed != -2.0)
                    {
                        this.bird.Speed = this.bird.Speed * 2;
                    }
                }
                else if (this.bird.Speed == 0.0)
                {
                    this.bird.Speed = -1.0;
                }
                else if (this.bird.Speed > 1.0)
                {
                    this.bird.Speed = this.bird.Speed / 2;
                }
                else
                {
                    this.bird.Speed = 0.0;
                }
            }
            else
            {
                this.isOver = true;
            }
            Obstacle spare = null;
            foreach (var item in obs)
            {
                if (item.Position == 0)
                {
                    spare = item;
                }
                else
                {
                    item.Position -= 1;
                }
            }
            if (spare != null)
            {
                this.obs.Remove(spare);
                this.Point++;
                if (this.Point == this.goal)
                {
                    this.isOver = true;
                    this.completed = true;
                }
            }
            if (this.Steps % 10 == 0)
            {
                this.obs.Add(new Obstacle(this.Size, this.Height, this.Steps));
            }
            this.Steps++;

        }
        private int CalculateBirdPosition()
        {
            int result = Convert.ToInt32(this.bird.GetPosition() - this.bird.Speed);
            if (result > this.Height - 1)
            {
                result = this.Height - 1;
                this.isOver = true;
            }
            else if (result < 0)
            {
                result = 0;
                this.isOver = true;
            }
            return result;
        }
        private void CheckBirdLifeSigns()
        {
            int obIdx = obs.Where(x => x.Position <= 4).Select(x => x.Position).FirstOrDefault();
            var ob = (from x in obs
                      where x.Position == obIdx
                      select x).FirstOrDefault();
            if (ob != null)
            {
                if ((ob.GetDirection() == 1 && this.bird.GetPosition() >= (this.Height - ob.GetLength())) || (ob.GetDirection() == 0 && this.bird.GetPosition() <= ob.GetLength()))
                {
                    this.isOver = true;
                }
            }
        }
        private void ScanKeyBoard()
        {
            if (Console.KeyAvailable)
            {
                this.key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.Spacebar)
                {
                    this.bird.Speed = 2;
                }
            }
        }
        private int GetPreviousHighScore()
        {
            if (File.Exists("record.xml"))
            {
                this.xdoc = XDocument.Load("record.xml");
                switch (this.gamemode)
                {
                    case GameMode.Easy:
                        return int.Parse((from x in xdoc.Descendants("Record")
                                          where x.Element("GameMode").Value == "Easy"
                                          select x.Element("HighScore")).FirstOrDefault().Value);
                    case GameMode.Normal:
                        return int.Parse((from x in xdoc.Descendants("Record")
                                          where x.Element("GameMode").Value == "Normal"
                                          select x.Element("HighScore")).FirstOrDefault().Value);
                    case GameMode.Hard:
                        return int.Parse((from x in xdoc.Descendants("Record")
                                          where x.Element("GameMode").Value == "Hard"
                                          select x.Element("HighScore")).FirstOrDefault().Value);
                    case GameMode.Impossible:
                        return int.Parse((from x in xdoc.Descendants("Record")
                                          where x.Element("GameMode").Value == "Impossible"
                                          select x.Element("HighScore")).FirstOrDefault().Value);
                    default:
                        return 9999;
                }
            }
            return 0;
        }
    }
}
