﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Flappy_Bird
{
    class Obstacle
    {
        Random rnd = new Random();
        public int Position { get; set; }
        private int Length;
        private byte Direction; //1 is upwards
        public Obstacle(int size, int height, int point)
        {
            this.Position = size - 4 ;
             GenLength(height, point);
            GenDirection();
        }
        private void GenLength(int height, int point)
        {
            this.Length = rnd.Next(height == 8 ? 1 : height / 4, height == 8 ? 3 : (height / 2) + 1);
        }

        private void GenDirection()
        {
            this.Direction = Convert.ToByte(rnd.Next(0,2));
         }

        public int GetLength()
        {
            return this.Length;
        }

        public int GetDirection()
        {
            return this.Direction;
        }
    }
}
