﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Flappy_Bird
{
    class Bird
    {
        private int Position;
        public double Speed { get; set; }
        public Bird(int height)
        {
            this.Position = height/2;
            this.Speed = 0.0;
        }
        public override string ToString()
        {
            return "O";
        }

        public int GetPosition()
        {
            return this.Position;
        }

        public void SetPosition(int value)
        {
            this.Position = value;
        }
    }
}
